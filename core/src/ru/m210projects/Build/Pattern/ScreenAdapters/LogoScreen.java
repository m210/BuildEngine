//This file is part of BuildGDX.
//Copyright (C) 2017-2018  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//BuildGDX is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//BuildGDX is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with BuildGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Build.Pattern.ScreenAdapters;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Render.Renderer;

public class LogoScreen extends SkippableAdapter {
	
	protected int nTile;
	protected float gTicks;
	protected float gShowTime;
	protected Runnable callback;
	
	public LogoScreen(BuildGame game, float gShowTime)
	{
		super(game);
		this.gShowTime = gShowTime;
	}
	
	public LogoScreen setTile(int nTile) {
		this.nTile = nTile;
		return this;
	}
	
	public LogoScreen setCallback(Runnable callback) {
		this.callback = callback;
		this.skipCallback = callback;
		return this;
	}

	@Override
	public void show() {
		this.gTicks = 0;
	}

	@Override
	public void draw(float delta) {
		
		if( (gTicks += delta) >= gShowTime && callback != null)
		{
			Gdx.app.postRunnable(callback);
			callback = null;
		}

		Renderer renderer = game.getRenderer();
		renderer.clearview(0);
		renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, nTile, 0, 0, 10 | 64);
	}

}
